package com.xqkj.commons.exporter;

import com.xqkj.commons.exporter.model.ExcelHeaderInfoVO;
import com.xqkj.commons.exporter.model.ExcelWriteExtInfoVO;
import com.xqkj.commons.exporter.model.ExcelWriteResult;
import com.xqkj.commons.model.HandleResult;
import org.apache.poi.ss.usermodel.Sheet;

import java.io.OutputStream;

/**
 * excel标题写组件--用于拔插扩展
 * @ Author     ：lwt-mac<br>
 * @ Date       ：2019/6/29 3:37 PM <br>
 * @ Description：<br>
 * @ Modified By：<br>
 * @Version: 1.000
 */
public interface ExcelHeaderRowWriter {

    HandleResult<ExcelWriteResult> doWriteHeaderRowDate(OutputStream outputStream,
                                                        Sheet sheet, ExcelHeaderInfoVO excelHeaderInfoVO,
                                                        ExcelWriteExtInfoVO excelWriteExtInfoVO);
}
