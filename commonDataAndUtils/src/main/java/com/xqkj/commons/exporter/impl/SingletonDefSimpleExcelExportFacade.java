package com.xqkj.commons.exporter.impl;

import com.xqkj.commons.exporter.SimpleExcelExportFacade;
import com.xqkj.commons.progress.model.RunInforModel;
import com.xqkj.commons.progress.model.RunInforQuery;

import java.util.List;

/**
 * 单例
 * @ Author     ：lwt-mac<br>
 * @ Date       ：2019/7/2 4:52 PM <br>
 * @ Description：<br>
 * @ Modified By：<br>
 * @Version: 1.000
 */
public class SingletonDefSimpleExcelExportFacade extends DefSimpleExcelExportFacade{

    public static volatile SimpleExcelExportFacade instance;

    private SingletonDefSimpleExcelExportFacade(){

    }

    public static SimpleExcelExportFacade getInstance(){
        if(instance==null){
            synchronized (SingletonDefSimpleExcelExportFacade.class){
                instance=new SingletonDefSimpleExcelExportFacade();
            }
        }
        return instance;
    }

    public static List<RunInforModel> listRunInfoModel(RunInforQuery query){
        return getInstance().queryRunInfoList(query);
    }

    public static RunInforModel getRunInfoModelByKey(String key){
        return getInstance().getRunInfoByKey(key);
    }
}
