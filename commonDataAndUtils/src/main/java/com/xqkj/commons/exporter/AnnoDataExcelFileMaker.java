package com.xqkj.commons.exporter;

import com.xqkj.annotation.MethodInteceptAnno;
import com.xqkj.commons.constant.IntecepterManagerNames;
import com.xqkj.commons.exporter.model.ExcelWriteExtInfoVO;
import com.xqkj.commons.exporter.model.FileMakeResult;
import com.xqkj.commons.model.BasicPageQuery;
import com.xqkj.commons.model.HandleResult;

/**
 * 先生成一个本地文件，然后将生成的excel数据刷如文件中。
 * @ Author     ：lwt-mac<br>
 * @ Date       ：2019/6/29 11:16 AM <br>
 * @ Description：<br>
 * @ Modified By：<br>
 * @Version: 1.000
 */
public interface AnnoDataExcelFileMaker {

    /**
     * pageListDateMaker 查询数据返回带有注解的数据映射模型分页列表，然后将这些数据写入到Excel 文件，并刷入到生成的本地文件中
     * @param filePath
     * @param fileName
     * @param extName
     * @param query
     * @param dateType
     * @param pageListDateMaker
     * @param excelWriteExtInfoVO
     * @return
     */
    @MethodInteceptAnno(managerName = IntecepterManagerNames.AnnoDataExcelFileMaker_MakeAndWriteAnnoExcelFile,
            argsNamesHolderClass = "com.xqkj.methodargs.AnnoDataExcelFileMaker_makeAndWriteAnnoExcelFile")
    <T,Q extends BasicPageQuery>
    HandleResult<FileMakeResult> makeAndWriteAnnoExcelFile(String filePath, String fileName, String extName,
                                                           Q query, Class<T> dateType,
                                                           PageListDateMaker<T,Q> pageListDateMaker,
                                                           ExcelWriteExtInfoVO excelWriteExtInfoVO);

    void setAnnoDataExcelFileWriter(AnnoDataExcelFileWriter annoDataExcelFileWriter);

    AnnoDataExcelFileWriter getAnnoDataExcelFileWriter();
}
