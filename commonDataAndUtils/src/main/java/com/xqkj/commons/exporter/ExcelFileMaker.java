package com.xqkj.commons.exporter;

import com.xqkj.annotation.MethodInteceptAnno;
import com.xqkj.commons.constant.IntecepterManagerNames;
import com.xqkj.commons.exporter.model.ExcelHeaderInfoVO;
import com.xqkj.commons.exporter.model.ExcelWriteExtInfoVO;
import com.xqkj.commons.exporter.model.FileMakeResult;
import com.xqkj.commons.model.BasicPageQuery;
import com.xqkj.commons.model.HandleResult;

/**
 * 先生成一个本地文件，然后将生成的excel数据刷如文件中。
 * @ Author     ：lwt-mac<br>
 * @ Date       ：2019/6/27 1:48 PM <br>
 * @ Description：<br>
 * @ Modified By：<br>
 * @Version: 1.000
 */
public interface ExcelFileMaker {

    /**
     * excelRowDataMaker 直接生成一个标准格式的分页数据列表，写入到excel中，并刷入到给定的文件目录。
     * @param filePath
     * @param fileName
     * @param extName
     * @param query
     * @param excelHeaderInfoVO
     * @param excelRowDataMaker
     * @param excelWriteExtInfoVO
     * @return
     */
    @MethodInteceptAnno(managerName = IntecepterManagerNames.ExcelFileMaker_MakeAndWriteExcelFile,
            argsNamesHolderClass = "com.xqkj.methodargs.ExcelFileMaker_makeAndWriteExcelFile")
    <Q extends BasicPageQuery>
    HandleResult<FileMakeResult> makeAndWriteExcelFile(String filePath, String fileName, String extName,
                                                       Q query, ExcelHeaderInfoVO excelHeaderInfoVO,
                                                       ExcelRowDataMaker<Q> excelRowDataMaker,
                                                       ExcelWriteExtInfoVO excelWriteExtInfoVO);



    void setExcelFileWriter(ExcelFileWriter ExcelFileWriter);

    ExcelFileWriter getExcelFileWriter();


}
