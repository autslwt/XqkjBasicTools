package com.xqkj.commons.exporter;

import java.util.List;

public interface ListDataMaker {
	<T> List<T> createListData();
}
