package com.xqkj.commons.exporter;

import com.xqkj.commons.exporter.model.ExcelRowInfoVO;
import com.xqkj.commons.model.PageInfoModel;
import com.xqkj.commons.model.BasicPageQuery;

/**
 * @ Author     ：lwt-mac<br>
 * @ Date       ：2019/6/11 2:00 PM <br>
 * @ Description：<br>
 * @ Modified By：<br>
 * @Version: 1.000
 */
public interface ExcelRowDataMaker<Q extends BasicPageQuery> {
    /**
     *
     * @param query
     * @return
     */
    PageInfoModel<ExcelRowInfoVO> queryData(Q query);
}
