package com.xqkj.commons.exporter;

import com.xqkj.commons.exporter.model.ExcelRowCellInfoVO;
import com.xqkj.commons.exporter.model.ExcelWriteExtInfoVO;
import com.xqkj.commons.exporter.model.ExcelWriteResult;
import com.xqkj.commons.model.HandleResult;
import org.apache.poi.ss.usermodel.Cell;

import java.io.OutputStream;

/**
 * excel表体单元格写组件--用于拔插扩展
 * @ Author     ：lwt-mac<br>
 * @ Date       ：2019/6/29 3:36 PM <br>
 * @ Description：<br>
 * @ Modified By：<br>
 * @Version: 1.000
 */
public interface ExcelRowCellWriter {

    HandleResult<ExcelWriteResult> doWriteRowCellDate(OutputStream outputStream,
                                                      Cell cell,
                                                      ExcelRowCellInfoVO excelRowCellInfoVO,
                                                      ExcelWriteExtInfoVO excelWriteExtInfoVO);
}
