package com.xqkj.commons.utils;

import org.apache.commons.lang3.StringUtils;

/**
 * @author lwt<br>
 * @description <br>
 * @date 2020/1/6
 */
public class XqkjStringUtils {

    public static String firstCharUp(String str){
        if(StringUtils.isBlank(str)){
            return str;
        }
        return str.substring(0,1).toUpperCase()+str.substring(1,str.length());
    }

}
