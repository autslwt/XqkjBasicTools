package com.xqkj.commons.importer;

import com.xqkj.commons.importer.model.ExcelHeaderReadConfig;
import com.xqkj.commons.importer.model.ExcelReadExtInfoVO;
import com.xqkj.commons.importer.model.ExcelReadResult;
import com.xqkj.commons.model.HandleResult;
import org.apache.poi.ss.usermodel.Sheet;

/**
 * @ Author     ：lwt-mac<br>
 * @ Date       ：2019/7/10 4:10 PM <br>
 * @ Description：<br>
 * @ Modified By：<br>
 * @Version: 1.000
 */
public interface ExcelHeaderRowReader {

    HandleResult<ExcelReadResult> doReadHeaderRowDate(Sheet sheet, ExcelHeaderReadConfig excelHeaderReadConfig,
                                                      ExcelReadExtInfoVO excelReadExtInfoVO);
}
