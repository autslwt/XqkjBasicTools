package com.xqkj.commons.importer.impl;

import com.xqkj.commons.constant.JavaValueTypeEnum;
import com.xqkj.commons.exceptions.BizException;
import com.xqkj.commons.importer.ExcelHeaderRowReader;
import com.xqkj.commons.importer.model.*;
import com.xqkj.commons.model.HandleResult;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

import java.util.ArrayList;
import java.util.List;

/**
 * @ Author     ：lwt-mac<br>
 * @ Date       ：2019/7/10 4:11 PM <br>
 * @ Description：<br>
 * @ Modified By：<br>
 * @Version: 1.000
 */
public class DefExcelHeaderRowReader implements ExcelHeaderRowReader {
    @Override
    public HandleResult<ExcelReadResult> doReadHeaderRowDate(Sheet sheet, ExcelHeaderReadConfig excelHeaderReadConfig,
                                                             ExcelReadExtInfoVO excelReadExtInfoVO) {
        ExcelReadResult excelReadResult=new ExcelReadResult();
        if(excelHeaderReadConfig==null || !excelHeaderReadConfig.isHasHeader()){
            return HandleResult.success(excelReadResult);
        }
        //
        ExcelRowReadInfoVO excelHeaderRowReadInfoVO=new ExcelRowReadInfoVO();
        List<ExcelRowCellReadInfoVO> cellInfoVOList=new ArrayList<>();
        excelHeaderRowReadInfoVO.setCellInfoVOList(cellInfoVOList);
        int cellIndex=excelHeaderReadConfig.getHeaderCellStartIndex();
        int rowIndex=excelHeaderReadConfig.getHeaderRowIndex();
        excelHeaderRowReadInfoVO.setCurrentRowIndex(rowIndex);
        Row headerRow=sheet.getRow(rowIndex);
        while (true){
            Cell cell = headerRow.getCell(cellIndex);
            if(cell==null){
                break;
            }
            CellType cellType = cell.getCellTypeEnum();
            if(CellType.BLANK.equals(cellType)){
                break;
            }
            //
            ExcelRowCellReadInfoVO excelRowCellReadInfoVO=new ExcelRowCellReadInfoVO();
            excelRowCellReadInfoVO.setCurrentCellIndex(cellIndex);
            String cellVal;
            if(CellType.STRING.equals(cellType)){
                cellVal=cell.getStringCellValue();
                if(StringUtils.isBlank(cellVal)){
                    break;
                }
            }else if(CellType.NUMERIC.equals(cellType)){
                double doubleCellVal=cell.getNumericCellValue();
                cellVal=String.valueOf(doubleCellVal);
            }else {
                throw new BizException("表头数据只允许文本或者数字");
            }
            //
            excelRowCellReadInfoVO.setValue(cellVal);
            excelRowCellReadInfoVO.setValueType(JavaValueTypeEnum.STRING.getName());
            cellInfoVOList.add(excelRowCellReadInfoVO);
            cellIndex++;
        }
        rowIndex++;
        excelReadResult.setNextRowIndex(rowIndex);
        excelReadResult.setExcelHeaderRowReadInfoVO(excelHeaderRowReadInfoVO);
        return HandleResult.success(excelReadResult);
    }
}
