package com.xqkj.commons.importer.model;

/**
 * @ Author     ：lwt-mac<br>
 * @ Date       ：2019/7/9 7:03 PM <br>
 * @ Description：<br>
 * @ Modified By：<br>
 * @Version: 1.000
 */
public class ExcelHeaderReadConfig {

    private boolean hasHeader;

    private int headerRowIndex;

    private int headerCellStartIndex;

    public boolean isHasHeader() {
        return hasHeader;
    }

    public void setHasHeader(boolean hasHeader) {
        this.hasHeader = hasHeader;
    }

    public int getHeaderRowIndex() {
        return headerRowIndex;
    }

    public void setHeaderRowIndex(int headerRowIndex) {
        this.headerRowIndex = headerRowIndex;
    }

    public int getHeaderCellStartIndex() {
        return headerCellStartIndex;
    }

    public void setHeaderCellStartIndex(int headerCellStartIndex) {
        this.headerCellStartIndex = headerCellStartIndex;
    }
}
