package com.xqkj.commons.importer.model;

import java.util.List;

/**
 * @ Author     ：lwt-mac<br>
 * @ Date       ：2019/7/9 4:31 PM <br>
 * @ Description：<br>
 * @ Modified By：<br>
 * @Version: 1.000
 */
public class ExcelReadResult {

    private int nextRowIndex;

    private ExcelRowReadInfoVO excelHeaderRowReadInfoVO;

    private ExcelRowReadInfoVO excelTmpBodyRowReadInfoVO;

    private ExcelRowCellReadInfoVO excelTmpRowCellReadInfoVO;

    private List<ExcelRowReadInfoVO> excelBodyRowReadInfoVOList;

    public int getNextRowIndex() {
        return nextRowIndex;
    }

    public void setNextRowIndex(int nextRowIndex) {
        this.nextRowIndex = nextRowIndex;
    }

    public ExcelRowReadInfoVO getExcelHeaderRowReadInfoVO() {
        return excelHeaderRowReadInfoVO;
    }

    public void setExcelHeaderRowReadInfoVO(ExcelRowReadInfoVO excelHeaderRowReadInfoVO) {
        this.excelHeaderRowReadInfoVO = excelHeaderRowReadInfoVO;
    }

    public List<ExcelRowReadInfoVO> getExcelBodyRowReadInfoVOList() {
        return excelBodyRowReadInfoVOList;
    }

    public void setExcelBodyRowReadInfoVOList(List<ExcelRowReadInfoVO> excelBodyRowReadInfoVOList) {
        this.excelBodyRowReadInfoVOList = excelBodyRowReadInfoVOList;
    }

    public ExcelRowReadInfoVO getExcelTmpBodyRowReadInfoVO() {
        return excelTmpBodyRowReadInfoVO;
    }

    public void setExcelTmpBodyRowReadInfoVO(ExcelRowReadInfoVO excelTmpBodyRowReadInfoVO) {
        this.excelTmpBodyRowReadInfoVO = excelTmpBodyRowReadInfoVO;
    }

    public ExcelRowCellReadInfoVO getExcelTmpRowCellReadInfoVO() {
        return excelTmpRowCellReadInfoVO;
    }

    public void setExcelTmpRowCellReadInfoVO(ExcelRowCellReadInfoVO excelTmpRowCellReadInfoVO) {
        this.excelTmpRowCellReadInfoVO = excelTmpRowCellReadInfoVO;
    }
}
