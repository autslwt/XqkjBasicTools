package com.xqkj.commons.importer;

import com.xqkj.commons.importer.model.*;
import com.xqkj.commons.model.HandleResult;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

/**
 * @ Author     ：lwt-mac<br>
 * @ Date       ：2019/7/9 9:48 PM <br>
 * @ Description：<br>
 * @ Modified By：<br>
 * @Version: 1.000
 */
public interface ExcelFileReaderCallBack {

    HandleResult<ExcelReadResult> callReadHeaderOver(Sheet sheet, ExcelHeaderReadConfig excelHeaderReadConfig,
                                                     ExcelReadExtInfoVO excelReadExtInfoVO,
                                                     HandleResult<ExcelReadResult> readResult);

    HandleResult<ExcelReadResult> callReadBodyRowOver(Row row, int cellStartIndex,
                                                      ExcelReadExtInfoVO excelReadExtInfoVO,
                                                      HandleResult<ExcelReadResult> readResult);

    HandleResult<ExcelReadResult> callReadExcelOver(Workbook workbook, String sheetName,
                                                    ExcelReadExtInfoVO excelReadExtInfoVO,
                                                    HandleResult<ExcelReadResult> readResult);

}
