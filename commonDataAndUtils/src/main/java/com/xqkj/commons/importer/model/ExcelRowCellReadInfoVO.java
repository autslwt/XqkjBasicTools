package com.xqkj.commons.importer.model;

/**
 * @ Author     ：lwt-mac<br>
 * @ Date       ：2019/7/10 3:20 PM <br>
 * @ Description：<br>
 * @ Modified By：<br>
 * @Version: 1.000
 */
public class ExcelRowCellReadInfoVO {

    private int currentCellIndex;

    private Object value;

    private String valueType;

    private String valueFormat;

    private String cellCode;

    public int getCurrentCellIndex() {
        return currentCellIndex;
    }

    public void setCurrentCellIndex(int currentCellIndex) {
        this.currentCellIndex = currentCellIndex;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public String getValueType() {
        return valueType;
    }

    public void setValueType(String valueType) {
        this.valueType = valueType;
    }

    public String getValueFormat() {
        return valueFormat;
    }

    public void setValueFormat(String valueFormat) {
        this.valueFormat = valueFormat;
    }

    public String getCellCode() {
        return cellCode;
    }

    public void setCellCode(String cellCode) {
        this.cellCode = cellCode;
    }
}
