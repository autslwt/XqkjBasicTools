package com.xqkj.commons.importer;

import com.xqkj.commons.basic.ComponentHolder;
import com.xqkj.commons.importer.model.*;
import com.xqkj.commons.model.HandleResult;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

/**
 * @ Author     ：lwt-mac<br>
 * @ Date       ：2019/7/9 4:20 PM <br>
 * @ Description：<br>
 * @ Modified By：<br>
 * @Version: 1.000
 */
public interface ExcelFileReader {


    HandleResult<ExcelReadResult> readExcelData(Workbook workbook, String sheetName,
                                                ExcelHeaderReadConfig excelHeaderReadConfig,
                                                ExcelBodyRowListReadConfig excelBodyRowListReadConfig,
                                                ExcelFileReaderCallBack excelFileReaderCallBack,
                                                ExcelReadExtInfoVO excelReadExtInfoVO);

    HandleResult<ExcelReadResult> readExcelHeader(Sheet sheet, ExcelHeaderReadConfig excelHeaderReadConfig,
                                                  ExcelFileReaderCallBack excelFileReaderCallBack,
                                                  ExcelReadExtInfoVO excelReadExtInfoVO);

    HandleResult<ExcelReadResult> readExcelRowList(Sheet sheet, ExcelBodyRowListReadConfig excelBodyRowListReadConfig,
                                                   ExcelRowReadInfoVO excelHeaderRowReadInfoVO,
                                                   ExcelFileReaderCallBack excelFileReaderCallBack,
                                                   ExcelReadExtInfoVO excelReadExtInfoVO);

    HandleResult<ExcelReadResult> readExcelRow(Row row, int cellStartIndex,ExcelRowReadInfoVO excelHeaderRowReadInfoVO,
                                               ExcelFileReaderCallBack excelFileReaderCallBack,
                                               ExcelReadExtInfoVO excelReadExtInfoVO);

    HandleResult<ExcelReadResult> readExcelRowCell(Cell cell, ExcelRowReadInfoVO excelHeaderRowReadInfoVO,
                                                   ExcelReadExtInfoVO excelReadExtInfoVO);

    /**
     * 组件持有容器代理
     * @param componentHolder
     */
    void setComponentHolder(ComponentHolder componentHolder);
}
