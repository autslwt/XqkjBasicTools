package com.xqkj.commons.importer.model;

/**
 * @ Author     ：lwt-mac<br>
 * @ Date       ：2019/7/9 4:32 PM <br>
 * @ Description：<br>
 * @ Modified By：<br>
 * @Version: 1.000
 */
public class ExcelReadExtInfoVO {

    /**
     *
     */
    private Integer sumCount;
    /**
     * 表头读实现类的注册名称--必须使用componentHoldHelper可以获取--默认使用ExcelProxFactory的注册方法注册
     */
    private String excelHeaderRowReaderName;
    /**
     * 单元格数据读实现类的注册名称--必须使用componentHoldHelper可以获取--默认使用ExcelProxFactory的注册方法注册
     */
    private String excelRowCellReaderName;

    public Integer getSumCount() {
        return sumCount;
    }

    public void setSumCount(Integer sumCount) {
        this.sumCount = sumCount;
    }

    public String getExcelHeaderRowReaderName() {
        return excelHeaderRowReaderName;
    }

    public void setExcelHeaderRowReaderName(String excelHeaderRowReaderName) {
        this.excelHeaderRowReaderName = excelHeaderRowReaderName;
    }

    public String getExcelRowCellReaderName() {
        return excelRowCellReaderName;
    }

    public void setExcelRowCellReaderName(String excelRowCellReaderName) {
        this.excelRowCellReaderName = excelRowCellReaderName;
    }
}
