package com.xqkj.commons.importer.model;

import java.util.List;

/**
 * @ Author     ：lwt-mac<br>
 * @ Date       ：2019/7/9 9:28 PM <br>
 * @ Description：<br>
 * @ Modified By：<br>
 * @Version: 1.000
 */
public class ExcelRowReadInfoVO {

    private int currentRowIndex;

    private List<ExcelRowCellReadInfoVO> cellInfoVOList;

    public int getCurrentRowIndex() {
        return currentRowIndex;
    }

    public void setCurrentRowIndex(int currentRowIndex) {
        this.currentRowIndex = currentRowIndex;
    }

    public List<ExcelRowCellReadInfoVO> getCellInfoVOList() {
        return cellInfoVOList;
    }

    public void setCellInfoVOList(List<ExcelRowCellReadInfoVO> cellInfoVOList) {
        this.cellInfoVOList = cellInfoVOList;
    }
}
