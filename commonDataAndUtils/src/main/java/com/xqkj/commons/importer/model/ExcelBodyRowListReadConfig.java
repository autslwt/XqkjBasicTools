package com.xqkj.commons.importer.model;

/**
 * @ Author     ：lwt-mac<br>
 * @ Date       ：2019/7/9 9:19 PM <br>
 * @ Description：<br>
 * @ Modified By：<br>
 * @Version: 1.000
 */
public class ExcelBodyRowListReadConfig {
    private int currentRowIndex;

    private int currentCellStartIndex;

    public int getCurrentRowIndex() {
        return currentRowIndex;
    }

    public void setCurrentRowIndex(int currentRowIndex) {
        this.currentRowIndex = currentRowIndex;
    }

    public int getCurrentCellStartIndex() {
        return currentCellStartIndex;
    }

    public void setCurrentCellStartIndex(int currentCellStartIndex) {
        this.currentCellStartIndex = currentCellStartIndex;
    }
}
