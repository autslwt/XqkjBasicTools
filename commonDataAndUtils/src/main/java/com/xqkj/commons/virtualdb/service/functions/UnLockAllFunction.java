package com.xqkj.commons.virtualdb.service.functions;

import com.xqkj.commons.virtualdb.model.VirtualDbLockInfoModel;
import org.apache.commons.collections4.MapUtils;

import java.util.function.Consumer;

/**
 * @author lwt<br>
 * @description <br>
 * @date 2020/1/15
 */
public class UnLockAllFunction implements Consumer<VirtualDbLockInfoModel> {

    private Consumer<String> unLockFunction;

    public UnLockAllFunction(Consumer<String> unLockFunction){
        this.unLockFunction=unLockFunction;
    }

    @Override
    public void accept(VirtualDbLockInfoModel virtualDbLockInfoModel) {
        if(virtualDbLockInfoModel.isMainIdxLocked()){
            unLockForOperate(virtualDbLockInfoModel.getMainIdxLockKey());
        }
        if(MapUtils.isNotEmpty(virtualDbLockInfoModel.getAttributeLockKeyMap())){
            virtualDbLockInfoModel.getAttributeLockKeyMap().entrySet().forEach(item->{
                if(item.getValue()!=null && item.getValue()){
                    unLockForOperate(item.getKey());
                }
            });
        }
    }

    private void unLockForOperate(String key){
        unLockFunction.accept(key);
    }
}
