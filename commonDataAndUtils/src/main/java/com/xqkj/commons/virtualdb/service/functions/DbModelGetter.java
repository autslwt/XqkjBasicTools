package com.xqkj.commons.virtualdb.service.functions;

import com.xqkj.commons.model.HandleResult;
import com.xqkj.commons.virtualdb.model.GetByUnionIdxParameterModel;
import com.xqkj.commons.virtualdb.model.VirtualDbAttributeLockInfoModel;
import com.xqkj.commons.virtualdb.model.VirtualDbLockInfoModel;
import org.apache.commons.collections4.CollectionUtils;

import java.util.List;
import java.util.function.Function;

/**
 * @author lwt<br>
 * @description <br>
 * @date 2020/1/15
 */
public class DbModelGetter<P> implements Function<VirtualDbLockInfoModel,HandleResult<P>> {

    private Function<GetByUnionIdxParameterModel,P> getByMainUidxFunction;

    private Function<GetByUnionIdxParameterModel,P> getByAttributeUidxFunction;

    public DbModelGetter(Function<GetByUnionIdxParameterModel,P> getByMainUidxFunction,
                         Function<GetByUnionIdxParameterModel,P> getByAttributeUidxFunction){
        this.getByMainUidxFunction=getByMainUidxFunction;
        this.getByAttributeUidxFunction=getByAttributeUidxFunction;
    }

    /**
     * 如果没有异常则返回 HandleResult.success ，如果没有数据则result的entry为null
     * @param virtualDbLockInfoModel
     * @return
     */
    @Override
    public HandleResult<P> apply(VirtualDbLockInfoModel virtualDbLockInfoModel) {
        //1.先使用主表唯一索引查询
        GetByUnionIdxParameterModel getByMainUidxParameter=new GetByUnionIdxParameterModel();
        getByMainUidxParameter.setTenantId(virtualDbLockInfoModel.getTenantId());
        getByMainUidxParameter.setTenantExtCode(virtualDbLockInfoModel.getTenantExtCode());
        getByMainUidxParameter.setUidxCode(virtualDbLockInfoModel.getMainIdxCode());
        getByMainUidxParameter.setUidxValue(virtualDbLockInfoModel.getMainIdxLockKey());
        getByMainUidxParameter.setUidxFiledNameValueMap(virtualDbLockInfoModel.getMainIdxFieldValueMap());

        P dbModel=getByMainUidxFunction.apply(getByMainUidxParameter);
        if(dbModel!=null){
            return HandleResult.success(dbModel);
        }
        List<VirtualDbAttributeLockInfoModel> lockInfoModelList=virtualDbLockInfoModel.getAttributeLockInfolList();
        if(CollectionUtils.isEmpty(lockInfoModelList)){
            return HandleResult.success(null);
        }
        //2.在使用属性唯一索引查询
        for(VirtualDbAttributeLockInfoModel tmpLockInfoModel:lockInfoModelList){
            GetByUnionIdxParameterModel getByAttributeUidxParameter=new GetByUnionIdxParameterModel();
            getByAttributeUidxParameter.setTenantId(virtualDbLockInfoModel.getTenantId());
            getByAttributeUidxParameter.setTenantExtCode(virtualDbLockInfoModel.getTenantExtCode());
            getByAttributeUidxParameter.setUidxCode(tmpLockInfoModel.getAttributeCode());
            getByAttributeUidxParameter.setUidxValue(tmpLockInfoModel.getAttributeValue());
            dbModel=getByAttributeUidxFunction.apply(getByAttributeUidxParameter);
            if(dbModel!=null){
                return HandleResult.success(dbModel);
            }
        }
        return HandleResult.success(null);
    }
}
