package com.xqkj.commons.virtualdb.model;

import lombok.Data;

/**
 * @author lwt<br>
 * @description <br>
 * @date 2020/1/15
 */
@Data
public class VirtualDbAttributeLockInfoModel {

    public VirtualDbAttributeLockInfoModel(){

    }

    private String attributeCode;

    private String attributeName;

    private String attributeValue;

    private String lockKey;

}
