package com.xqkj.commons.virtualdb.model;

import lombok.Data;

/**
 * @author lwt<br>
 * @description <br>
 * @date 2020/1/15
 */
@Data
public class VirtualDbNormalShadingKeysModel {

    public VirtualDbNormalShadingKeysModel(){

    }

    private Long pkId;

    private Long tenantId;
}
