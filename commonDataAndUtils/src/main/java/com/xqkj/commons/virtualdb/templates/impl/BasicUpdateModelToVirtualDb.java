package com.xqkj.commons.virtualdb.templates.impl;

import com.xqkj.commons.model.HandleResult;

import java.util.function.Function;

/**
 * @author lwt<br>
 * @description <br>
 * @date 2020/1/16
 */
public abstract class BasicUpdateModelToVirtualDb<P> {

    protected Function<P,Boolean> delayCleanCacheFunction;
    protected Function<P,Boolean> cleanOrResetCacheFunction;

    protected HandleResult<Integer> updateModelAndTryCleanCache(P model){
        if(delayCleanCacheFunction!=null){
            delayCleanCacheFunction.apply(model);
        }
        HandleResult<Integer> handleResult=doUpdateModel(model);
        if(!handleResult.isSuccess()){
            return handleResult;
        }
        if(cleanOrResetCacheFunction!=null){
            cleanOrResetCacheFunction.apply(model);
        }
        return handleResult;
    }

    protected  abstract HandleResult<Integer> doUpdateModel(P model);

}
