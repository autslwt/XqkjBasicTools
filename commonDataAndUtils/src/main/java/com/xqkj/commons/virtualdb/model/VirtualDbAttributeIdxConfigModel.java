package com.xqkj.commons.virtualdb.model;

import lombok.Data;
import org.apache.commons.collections4.CollectionUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author lwt<br>
 * @description <br>
 * @date 2020/1/15
 */
@Data
public class VirtualDbAttributeIdxConfigModel {

    public VirtualDbAttributeIdxConfigModel(){

    }

    private String configCode;

    private String configName;

    private List<String> uidxModelFiledNameList;

    private boolean attributeMustInConfig=false;

    private Long tenantId;

    private String tenantExtCode;

    private List<VirtualDbAttributeIdxConfigValueModel> configValueList;

    private Map<String,VirtualDbAttributeIdxConfigValueModel> configValueMap=new HashMap<>();


    public Map<String, VirtualDbAttributeIdxConfigValueModel> getConfigValueMap(){
        if(configValueMap!=null && !configValueMap.isEmpty()){
            return configValueMap;
        }
        if(CollectionUtils.isEmpty(configValueList)){
            return configValueMap;
        }
        Map<String, VirtualDbAttributeIdxConfigValueModel> tmpConfigValueMap=new HashMap<>();
        configValueList.forEach(item->tmpConfigValueMap.put(item.getAttributeCode(),item));
        configValueMap=tmpConfigValueMap;
        return configValueMap;
    }
}
