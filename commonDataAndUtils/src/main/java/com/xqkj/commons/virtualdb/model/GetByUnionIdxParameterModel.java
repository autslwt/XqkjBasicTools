package com.xqkj.commons.virtualdb.model;

import lombok.Data;

import java.util.Map;

/**
 * @author lwt<br>
 * @description <br>
 * @date 2020/1/15
 */
@Data
public class GetByUnionIdxParameterModel {

    public GetByUnionIdxParameterModel(){

    }

    Long tenantId;

    String tenantExtCode;

    String uidxCode;

    String uidxValue;

    Map<String,Object> uidxFiledNameValueMap;
}
