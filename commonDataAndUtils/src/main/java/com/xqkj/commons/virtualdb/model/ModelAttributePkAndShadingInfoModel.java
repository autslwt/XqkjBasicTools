package com.xqkj.commons.virtualdb.model;

import lombok.Data;

/**
 * @author lwt<br>
 * @description <br>
 * @date 2020/1/16
 */
@Data
public class ModelAttributePkAndShadingInfoModel {

    private Long pkId;

    private Long parentPkId;

    private Long tenantId;

    public ModelAttributePkAndShadingInfoModel(){

    }
}
