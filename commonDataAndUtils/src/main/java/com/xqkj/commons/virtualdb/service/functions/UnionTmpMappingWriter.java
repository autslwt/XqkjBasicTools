package com.xqkj.commons.virtualdb.service.functions;

import com.xqkj.commons.model.HandleResult;
import com.xqkj.commons.virtualdb.model.UnionIdxMappingSvModel;
import com.xqkj.commons.virtualdb.model.VirtualDbLockInfoModel;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

/**
 * @author lwt<br>
 * @description <br>
 * @date 2020/1/15
 */
@Slf4j
public class UnionTmpMappingWriter implements Function<VirtualDbLockInfoModel,HandleResult<String>> {

    private Function<UnionIdxMappingSvModel,Integer> insertUidxMappingFunction;

    private Function<List<UnionIdxMappingSvModel>,Integer> batchInsertUidxMappingFunction;

    public UnionTmpMappingWriter(Function<UnionIdxMappingSvModel,Integer> insertUidxMappingFunction,
            Function<List<UnionIdxMappingSvModel>,Integer> batchInsertUidxMappingFunction){
        this.insertUidxMappingFunction=insertUidxMappingFunction;
        this.batchInsertUidxMappingFunction=batchInsertUidxMappingFunction;
    }

    @Override
    public HandleResult<String> apply(VirtualDbLockInfoModel virtualDbLockInfoModel) {
        try{
            List<UnionIdxMappingSvModel> idxMappingModelList=new ArrayList<>();
            if(StringUtils.isNotBlank(virtualDbLockInfoModel.getMainIdxLockKey())){
                UnionIdxMappingSvModel unionIdxMappingModel=new UnionIdxMappingSvModel();
                unionIdxMappingModel.setUidxCode(virtualDbLockInfoModel.getMainIdxCode());
                unionIdxMappingModel.setUidxName(virtualDbLockInfoModel.getMainIdxName());
                unionIdxMappingModel.setUidxValue(virtualDbLockInfoModel.getMainIdxLockKey());
                unionIdxMappingModel.setMappedPkId(virtualDbLockInfoModel.getPkId());
                unionIdxMappingModel.setTenantId(virtualDbLockInfoModel.getTenantId());
                unionIdxMappingModel.setTenantExtCode(virtualDbLockInfoModel.getMainIdxCode());
                idxMappingModelList.add(unionIdxMappingModel);
                //
                if(CollectionUtils.isEmpty(virtualDbLockInfoModel.getAttributeLockInfolList())){
                    insertUidxMappingFunction.apply(unionIdxMappingModel);
                    return HandleResult.success("");
                }
            }
            //
            virtualDbLockInfoModel.getAttributeLockInfolList().forEach(item->{
                UnionIdxMappingSvModel tmpIdxMappingModel=new UnionIdxMappingSvModel();
                tmpIdxMappingModel.setUidxCode(item.getAttributeCode());
                tmpIdxMappingModel.setUidxName(item.getAttributeName());
                tmpIdxMappingModel.setUidxValue(item.getAttributeValue());
                tmpIdxMappingModel.setMappedPkId(virtualDbLockInfoModel.getPkId());
                tmpIdxMappingModel.setTenantId(virtualDbLockInfoModel.getTenantId());
                tmpIdxMappingModel.setTenantExtCode(virtualDbLockInfoModel.getTenantExtCode());
                idxMappingModelList.add(tmpIdxMappingModel);
            });
            if(idxMappingModelList.size()==1){
                insertUidxMappingFunction.apply(idxMappingModelList.get(0));
                return HandleResult.success("");
            }
            batchInsertUidxMappingFunction.apply(idxMappingModelList);
            return HandleResult.success("");
        }catch (Exception ex){
            log.error("insertUnionTmpMappings idxValue=[{}],exception=[{}]",virtualDbLockInfoModel.getMainIdxLockKey(),
                    ex.getMessage(), ex);
            return HandleResult.failed(ex.getMessage());
        }
    }
}
