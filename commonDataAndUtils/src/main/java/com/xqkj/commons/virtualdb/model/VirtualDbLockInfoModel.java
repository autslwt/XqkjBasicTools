package com.xqkj.commons.virtualdb.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author lwt<br>
 * @description <br>
 * @date 2020/1/15
 */
@Data
public class VirtualDbLockInfoModel {

    public VirtualDbLockInfoModel(){

    }

    private boolean needLock=false;

    private Long pkId;

    private Long tenantId;

    private String tenantExtCode;

    private String mainIdxCode;

    private String mainIdxName;

    private String mainIdxLockKey;

    private boolean isMainIdxLocked=false;

    private Map<String,Object> mainIdxFieldValueMap;

    private List<VirtualDbAttributeLockInfoModel> attributeLockInfolList=new ArrayList<>();

    Map<String,Boolean> attributeLockKeyMap=new HashMap<>();
}
