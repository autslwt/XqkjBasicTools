package com.xqkj.commons.virtualdb.templates;

import com.xqkj.commons.model.HandleResult;
import com.xqkj.commons.model.basicdo.BasicDo;
import com.xqkj.commons.virtualdb.model.ModelBasicAttributeSvModel;
import com.xqkj.commons.virtualdb.model.VirtualDbAttributeIdxConfigModel;
import com.xqkj.commons.virtualdb.model.VirtualDbNormalShadingKeysModel;

import java.util.Map;

/**
 * @author lwt<br>
 * @description <br>
 * @date 2020/1/16
 */
public interface AddModelToVirtualDb<P extends BasicDo> {

    HandleResult<VirtualDbNormalShadingKeysModel> insertModel(P model, Long tenantId, String tenantExtCode,
                                                              Map<String,ModelBasicAttributeSvModel> codeToAttributeMap,
                                                              VirtualDbAttributeIdxConfigModel attributeIdxConfig);
}
