package com.xqkj.commons.virtualdb.model;

import lombok.Data;

/**
 * @author lwt<br>
 * @description <br>
 * @date 2020/1/15
 */
@Data
public class VirtualDbAttributeIdxConfigValueModel {

    public VirtualDbAttributeIdxConfigValueModel(){

    }

    private String attributeCode;

    private String attributeName;

    private String attributeType;
    /**
     * 0.不索引，用户级唯一
     * 1.索引，租户-租户扩展code下唯一
     */
    private int attributeIdxType;

}
