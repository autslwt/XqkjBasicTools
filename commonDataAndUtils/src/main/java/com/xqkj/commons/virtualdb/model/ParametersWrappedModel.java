package com.xqkj.commons.virtualdb.model;

import lombok.Data;

import java.util.Map;

/**
 * @author lwt<br>
 * @description <br>
 * @date 2020/1/15
 */
@Data
public class ParametersWrappedModel<P> {

    public ParametersWrappedModel(){
    }

    private P parameter;

    private VirtualDbAttributeIdxConfigModel attributeIdxConfig;

    private Map<String,ModelBasicAttributeSvModel> codeToAttributeMap;

    private Long tenantId;

    private String tenantExtCode;
}
