package com.xqkj.commons.virtualdb.service.functions;

import com.alibaba.fastjson.JSON;
import com.xqkj.commons.model.HandleResult;
import lombok.extern.slf4j.Slf4j;

import java.util.function.Function;

/**
 * @author lwt<br>
 * @description <br>
 * @date 2020/1/15
 */
@Slf4j
public class ModelInfoWriter<P> implements Function<P,HandleResult<Integer>> {


    private Function<P,Integer> writeModelFunction;

    public ModelInfoWriter(Function<P,Integer> writeModelFunction){
        this.writeModelFunction=writeModelFunction;
    }

    @Override
    public HandleResult<Integer> apply(P model) {
        try{
            Integer rsInt=writeModelFunction.apply(model);
            return HandleResult.success(rsInt);
        }catch (Exception ex){
            log.error("ModelInfoWriter.apply--insertModel exception=[{}],model=[{}]",ex.getMessage(),
                    JSON.toJSONString(model),ex);
            return HandleResult.failed(ex.getMessage());
        }
    }
}
