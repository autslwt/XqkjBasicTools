package com.xqkj.commons.virtualdb.templates;

import com.xqkj.commons.model.HandleResult;
import com.xqkj.commons.virtualdb.model.ModelBasicAttributeSvModel;
import com.xqkj.commons.virtualdb.model.VirtualDbAttributeIdxConfigModel;

/**
 * @author lwt<br>
 * @description <br>
 * @date 2020/1/16
 */
public interface UpdateAttributeToVirtualDb {

    HandleResult<Integer> updateModelAttribute(ModelBasicAttributeSvModel modelAttribute, Long tenantId, String tenantExtCode,
                                               VirtualDbAttributeIdxConfigModel attributeIdxConfig);
}
