package com.xqkj.commons.virtualdb.templates;

import com.xqkj.commons.model.HandleResult;
import com.xqkj.commons.model.basicdo.BasicDo;
import com.xqkj.commons.virtualdb.model.VirtualDbAttributeIdxConfigModel;

/**
 * @author lwt<br>
 * @description <br>
 * @date 2020/1/16
 */
public interface UpdateModelToVirtualDb<P extends BasicDo> {

    HandleResult<Integer> updateModelWithOutUidx(P model, Long tenantId, String tenantExtCode,
                                                 VirtualDbAttributeIdxConfigModel attributeIdxConfig);

    HandleResult<Integer> updateModelUidxFileds(P model, Long tenantId, String tenantExtCode,
                                                VirtualDbAttributeIdxConfigModel attributeIdxConfig);

}
