package com.xqkj.commons.virtualdb.service.functions;

import com.xqkj.commons.virtualdb.model.ParametersWrappedModel;

import java.util.function.Function;

/**
 * @author lwt<br>
 * @description <br>
 * @date 2020/1/15
 */
public class PkIdGenerater<P> implements Function<ParametersWrappedModel<P>, Long> {
    @Override
    public Long apply(ParametersWrappedModel<P> pParametersWrappedModel) {
        return System.currentTimeMillis();
    }
}
