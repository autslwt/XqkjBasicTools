package com.xqkj.commons.virtualdb.model;

import lombok.Data;

/**
 * @author lwt<br>
 * @description <br>
 * @date 2020/1/15
 */
@Data
public class VirtualDbLockResultModel {

    public VirtualDbLockResultModel(){

    }

    private boolean isLocked=false;

    private Integer resultCode;

    private String resultMsg;

}
