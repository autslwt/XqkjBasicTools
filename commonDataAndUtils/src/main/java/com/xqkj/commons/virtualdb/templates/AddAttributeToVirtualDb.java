package com.xqkj.commons.virtualdb.templates;

import com.xqkj.commons.model.HandleResult;
import com.xqkj.commons.virtualdb.model.ModelAttributePkAndShadingInfoModel;
import com.xqkj.commons.virtualdb.model.ModelBasicAttributeSvModel;
import com.xqkj.commons.virtualdb.model.VirtualDbAttributeIdxConfigModel;

/**
 * @author lwt<br>
 * @description <br>
 * @date 2020/1/16
 */
public interface AddAttributeToVirtualDb {

    HandleResult<ModelAttributePkAndShadingInfoModel> insertModelAttribute(ModelBasicAttributeSvModel attributeModel, Long tenantId, String tenantExtCode,
                                                                           VirtualDbAttributeIdxConfigModel attributeIdxConfig);


}
