package com.xqkj.commons.virtualdb.model;

import com.xqkj.commons.model.basicso.BasicSo;
import lombok.Data;

/**
 * @author lwt<br>
 * @description <br>
 * @date 2020/1/15
 */
@Data
public class UnionIdxMappingSvModel extends BasicSo {

    public UnionIdxMappingSvModel(){

    }

    private String uidxCode;

    private String uidxName;

    private String uidxValue;

    private Long mappedPkId;

    /**
     *租户id
     */
    private Long tenantId;
    /**
     *租户的的扩展编号--例如一个租户可以有多个app，那么code可以区分不同的app
     * 或者租户也可以是sass服务商，这个code可以区分租户的子客户--不过正常的话应该不支持子客户
     */
    private String tenantExtCode;
}
