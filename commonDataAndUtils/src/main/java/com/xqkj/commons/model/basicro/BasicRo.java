package com.xqkj.commons.model.basicro;

import java.io.Serializable;
import java.util.Date;

/**
 * @author lwt<br>
 * @description <br>
 * @date 2020/1/6
 */
public class BasicRo implements Serializable{

    private static final long serialVersionUID=1L;

    /**
     * 主键
     */
    private Long pkId;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 修改时间
     */
    private Date updateTime;


    public Long getPkId() {
        return pkId;
    }

    public void setPkId(Long pkId) {
        this.pkId = pkId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}
