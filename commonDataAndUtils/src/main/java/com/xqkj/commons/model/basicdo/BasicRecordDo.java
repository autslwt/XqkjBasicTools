package com.xqkj.commons.model.basicdo;

/**
 * @author lwt<br>
 * @description <br>
 * @date 2020/1/6
 */
public class BasicRecordDo extends BasicDo {

    /**
     * 初始化人员编号
     */
    private String create_person_no;
    /**
     * 初始化人员姓名
     */
    private String createPerson;
    /**
     * 最后修改人员姓名
     */
    private String updatePersonNo;
    /**
     * 最后修改人员编号
     */
    private String updatePerson;

    public String getCreate_person_no() {
        return create_person_no;
    }

    public void setCreate_person_no(String create_person_no) {
        this.create_person_no = create_person_no;
    }

    public String getCreatePerson() {
        return createPerson;
    }

    public void setCreatePerson(String createPerson) {
        this.createPerson = createPerson;
    }

    public String getUpdatePersonNo() {
        return updatePersonNo;
    }

    public void setUpdatePersonNo(String updatePersonNo) {
        this.updatePersonNo = updatePersonNo;
    }

    public String getUpdatePerson() {
        return updatePerson;
    }

    public void setUpdatePerson(String updatePerson) {
        this.updatePerson = updatePerson;
    }
}
