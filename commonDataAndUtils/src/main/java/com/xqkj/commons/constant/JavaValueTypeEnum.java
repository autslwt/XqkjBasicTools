package com.xqkj.commons.constant;

/**
 * @ Author     ：lwt-mac<br>
 * @ Date       ：2019/7/10 4:02 PM <br>
 * @ Description：<br>
 * @ Modified By：<br>
 * @Version: 1.000
 */
public enum  JavaValueTypeEnum {

    STRING(0,"String","String"),
    DOUBLE(1,"double","double"),
    ;

    private int code;
    private String name;
    private String desc;

    private JavaValueTypeEnum(int code,String name,String desc){
        this.code=code;
        this.name=name;
        this.desc=desc;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
