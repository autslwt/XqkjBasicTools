package com.xqkj.commons.constant;

/**
 * @ Author     ：lwt-mac<br>
 * @ Date       ：2019/7/10 7:25 PM <br>
 * @ Description：<br>
 * @ Modified By：<br>
 * @Version: 1.000
 */
public enum  HandleResultCode {

    SUCCESS(0,"处理成功"),
    FAILED(1,"处理失败"),
    //excel处理
    NO_DATE_READ(101,"处理失败"),
    //db 唯一索引已经存在
    DATA_IDX_EXISTED(201,"唯一索引已经存在"),
    DATA_NOT_EXISTED(202,"待处理数据不存在"),
    ;

    private int code;
    private String desc;

    private HandleResultCode(int code,String desc){
        this.code=code;
        this.desc=desc;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
