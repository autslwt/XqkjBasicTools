package com.xqkj.commons.test.handler;

import com.xqkj.commons.exporter.ExcelRowDataMaker;
import com.xqkj.commons.exporter.handler.BasicNormalDataExcelExportHandler;
import com.xqkj.commons.exporter.model.ExcelHeaderInfoVO;
import com.xqkj.commons.exporter.model.ExcelWriteExtInfoVO;
import com.xqkj.commons.exporter.model.ExportExcelRequestVO;
import com.xqkj.commons.exporter.model.FileUpLoadExtInfoVO;
import com.xqkj.commons.test.model.TestPageQuery;

/**
 * @ Author     ：lwt-mac<br>
 * @ Date       ：2019/7/6 10:40 AM <br>
 * @ Description：<br>
 * @ Modified By：<br>
 * @Version: 1.000
 */
public class TestNormalDataExcelExportHandler extends BasicNormalDataExcelExportHandler<TestPageQuery> {

    @Override
    protected ExcelHeaderInfoVO getExcelHeaderInfoVO(ExportExcelRequestVO requestVO) {
        return null;
    }

    @Override
    protected ExcelRowDataMaker<TestPageQuery> getExcelRowDataMaker(ExportExcelRequestVO requestVO) {
        return null;
    }

    @Override
    protected TestPageQuery getPageQueryByRequest(ExportExcelRequestVO requestVO) {
        return null;
    }

    @Override
    protected ExcelWriteExtInfoVO getExcelWriteExtInfoVO(ExportExcelRequestVO requestVO) {
        return null;
    }

    @Override
    protected FileUpLoadExtInfoVO getFileUpLoadExtInfoVO(ExportExcelRequestVO requestVO) {
        return null;
    }

    @Override
    public String handlerKey() {
        return null;
    }
}
